package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "thebill",
	Short: "borabill",
	Long:  `senhor bills`,
}

var randomCmd = &cobra.Command{
	Use:   "bilada",
	Short: "bill of bills",
	Long:  `this is the true power of bilada`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("its a bilada")
	},
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Whoops. There was an error while executing your CLI '%s'", err)
		os.Exit(1)
	}
}
func init() {
	rootCmd.AddCommand(randomCmd)
}
